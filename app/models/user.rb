class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :exercises
  has_many :friendships
  has_many :friends, through: :friendships, class_name: 'User'
  has_one :room
  has_many :messages


  after_create :create_chat_room

  validates_presence_of :first_name, :last_name

  self.per_page = 10

  def full_name
    "#{first_name} #{last_name}"
  end

  def follows_or_same?(new_friend)
    return true if equal?(new_friend)

    friendships.map(&:friend).include?(new_friend)
  end

  def current_friendship(friend)
    friendships.find_by(friend: friend)
  end

  class << self
    def search_by_name(name)
      query = find_by_name_sql(name.split(' '))
      where(query).order(:first_name)
    end

    def find_by_name_sql(names_array)
      if names_array.size == 1
        return "first_name LIKE '%#{names_array[0]}' or last_name LIKE '%#{names_array[0]}'"
      end

      <<-SQL
      first_name LIKE '%#{names_array[0]}' or first_name LIKE '%#{names_array[1]}'
      or last_name LIKE ? '%#{names_array[1]}' or last_name LIKE '%#{names_array[1]}'
      SQL
    end
  end

  private_class_method :find_by_name_sql

  private

  def create_chat_room
    hyphenated_username = full_name.sub(' ', '-')
    Room.create(name: hyphenated_username, user_id: self.id)
  end
end
