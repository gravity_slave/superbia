class Exercise < ApplicationRecord
  belongs_to :user

  alias_attribute :workout_details, :workout
  alias_attribute :activity_date, :workout_date

  validates_numericality_of :duration_in_min, greater_than: 0.0
  validates_presence_of :workout_details, :activity_date

  default_scope do
    where('workout_date > ?', 7.days.ago)
      .order(workout_date: :desc)
  end
end
