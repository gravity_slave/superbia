require 'rails_helper'
RSpec.feature 'signed in user can see only sign out link' do
  before do
    @fred =User.create!(first_name: "Fred", last_name: "Flinstone" ,email: "fred@example.com", password: "password")

  end

  scenario "can see only sign out" do
    visit "/"

    click_link("Sign in")

    fill_in "Email", with: @fred.email
    fill_in "Password", with: "password"
    click_button("Log in")

    expect(page).to have_link("Sign out")
    expect(page).not_to have_link("Sign in")
    expect(page).not_to have_link("Sign up")
  end
end