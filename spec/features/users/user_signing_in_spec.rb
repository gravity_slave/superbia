require 'rails_helper'
RSpec.feature "Signing in" do
  before do
    @fred =User.create(first_name: "John", last_name: "Doe" ,email: "fred@email.com", password: "password")
  end

  scenario "User can sign in" do
    visit "/"
    click_link("Sign in")
    fill_in "Email", with:  @fred.email
    fill_in "Password", with: @fred.password
    click_button "Log in"
    expect(page).to have_content("Signed in successfully.")
    expect(page).to have_content("Signed in as #{@fred.email}")
  end





end